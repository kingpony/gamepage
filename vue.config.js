const url = 'http://192.168.124.2:10010/na-admin'
// 基础路径，发布前修改这里,当前配置打包出来的资源都是相对路径
const webpack = require('webpack')
let publicPath = './'
module.exports = {
    publicPath: publicPath,
    lintOnSave: true,
    productionSourceMap: false,
    css: {
        // 忽略 CSS order 顺序警告
        extract: {ignoreOrder: true}
    },
    chainWebpack: config => {
        const entry = config.entry('app')
        entry
            .add('babel-polyfill')
            .end()
        entry
            .add('classlist-polyfill')
            .end()
    },
    // 配置转发代理
    devServer: {
        proxy: {
            '/api': {
                target: url,
                changOrigin: true,
                ws: true,
                pathRewrite: {
                    '/api': ''
                }
            }
        }
    }
}
