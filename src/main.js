import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Ant from 'ant-design-vue'
import 'ant-design-vue/dist/antd.css';
import VueAxios from 'vue-axios'
import axios from './router/meaxios'
import VueCookies from 'vue-cookies'
import Global from "./components/Global";

window.axios = axios
Vue.use(VueAxios, axios)
Vue.use(Ant);
Vue.prototype.GLOBAL = Global;
Vue.use(VueCookies);
new Vue({
    router,
    render: h => h(App)
}).$mount('#app')
