import request from '../router/meaxios'
export const loginByUsername = (username, password, code) => {
    const grant_type = 'password'
    let data = {
        username: username,
        password: password,
        checkcode: code
    };

    return request({
        url: '/api/login',
        method: 'post',
        // 参数传递，@RequestBody用data，而@Requestparam使用 param
        data: data
    });
}
