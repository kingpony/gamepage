import Vue from 'vue'
import VueRouter from 'vue-router'
import poetry from '../views/poetry/poetry'
import childpoetry from "../views/poetry/child/childpoetry";
import hello from "../components/HelloWorld"
import login from "../components/login/login"
import register from "../components/register/register"

// 设置登录过期时间
let EXPIRESTIME = 3000000

const routerPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
    return routerPush.call(this, location).catch(error => error)
}
Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        redirect: '/hello',
    },
    {
        path: '/register',
        name: 'register',
        component: register,
        meta: {
            notLogin: true    // 添加该字段，表示进入这个路由是不需要登录的
        }
    },
    {
        path: '/login',
        name: 'login',
        component: login,
        meta: {
            notLogin: true    // 添加该字段，表示进入这个路由是不需要登录的
        }
    },
    {
        path: '/hello',
        name: 'hello',
        component: hello,
        children:[
            {
                path: '/poetry',
                name: 'poetry',
                component: poetry,
                children: [
                    {
                        path: '/',
                        name: 'cpoetry',
                        component: childpoetry
                    }
                ]
            },
            {
                path: '/piechar',
                name: 'piechar',
                component: () => import(/* webpackChunkName: "about" */ '../views/piechar.vue')
            },
            {
                path: '/about',
                name: 'about',
                component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
            }
        ]
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})
// 路由守卫
router.beforeEach((to, from, next) => {
    if (to.matched.some(res => res.meta.notLogin)) {//判断是否需要登录
        console.log("此处不需要校验")
        next()
    } else {
        let item = localStorage.getItem("token");
        if (item) {
            item = JSON.parse(item);
            let date = new Date().getTime();
            // token时常过期处理
            if (date - item.startTime < EXPIRESTIME) {
                // TODO 需要每次更新一下item内的时间吗？后台使用的token，并没有给更新所以这个也不用更新
                next()
            } else {
                localStorage.removeItem('token');
                next({
                    path: '/login',
                    query: {
                        returnURL: to.path
                    }
                })
            }
        } else {
            next({
                path: '/login',
                query: {
                    returnURL: to.path
                }
            })
        }
    }
});

export default router
