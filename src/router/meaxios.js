import axios from 'axios'
import it from "element-ui/src/locale/lang/it";

axios.defaults.timeout = 30000
/*axios.defaults.validateStatus = function (status) {   Meaxios.js
   return status >= 200 && status <= 500 // 默认的
 }*/

axios.defaults.baseURL = 'http://192.168.124.2:8080' //设置统一路径前缀
// 跨域请求，允许保存cookie
axios.defaults.withCredentials = true
// NProgress Configuration
/*NProgress.configure({
  showSpinner: false
})*/

axios.interceptors.request.use(config => {
    let item = localStorage.getItem('token');
    if (item) {
        item = JSON.parse(item); //重新从json格式变为了object
        config.headers.Authorization = item.value;
    }
    return config
});


// HTTPrequest拦截
/* axios.interceptors.request.use(config => {
  NProgress.start() // start progress bar
  const isToken = (config.headers || {}).isToken === false
  let token =  store.getters.access_token
  if (token && !isToken) {
    config.headers['Authorization'] = 'Bearer ' + token// token
  }
  // headers中配置serialize为true开启序列化
  if (config.methods === 'post' && config.headers.serialize) {
    config.data = serialize(config.data)
    delete config.data.serialize
  }

  // 处理get 请求的数组 springmvc 可以处理
  if (config.method === 'get') {
    config.paramsSerializer = function (params) {
      return qs.stringify(params, { arrayFormat: 'repeat' })
    }
  }

  return config
}, error => {
  return Promise.reject(error)
})*/

// HTTPresponse拦截
/* axios.interceptors.response.use(res => {
  NProgress.done()
  const status = Number(res.status) || 200
  const message = res.data.msg || errorCode[status] || errorCode['default']
  if (status === 401) {
    store.dispatch('FedLogOut').then(() => {
      router.push({path: '/login'})
    })
    return
  }

  if (status !== 200 || res.data.code === 1) {
    Message({
      message: message,
      type: 'error'
    })
    return Promise.reject(new Error(message))
  }

  return res
}, error => {
  NProgress.done()
  return Promise.reject(new Error(error))
})*/

export default axios
